package cz.filek.dbm2;

import java.net.URI;

public class Utils {

    /**
     * Possible data types
     */
    enum DataType {
        STRING,
        INTEGER,
        DOUBLE,
        LONG,
        URI,
    }

    /**
     * Method predicts data type based on input string
     *
     * @param input input string
     * @return predicted data type, if not found returns string
     */
    public static DataType predictDataType(String input) {
        // If input contains . try to parse it as double
        if (input.contains(".")) {
            try {
                double dbl = Double.parseDouble(input);
                return DataType.DOUBLE;
            } catch (Exception ex) {/* Empty catch <3 */}
        }

        // Try integer
        try {
            int integer = Integer.parseInt(input);
            return DataType.INTEGER;
        } catch (Exception ex) {/* Empty catch <3 */}

        // Integer failed, maybe long will fit
        try {
            long lng = Long.parseLong(input);
            return DataType.LONG;
        } catch (Exception ex) {/* Empty catch <3 */}

        // Is it URI?
        try{
            if(input.contains(":") ||  input.contains(".") || input.contains("/")) {
                URI uri = new URI(input);
                return DataType.URI;
            }
        } catch (Exception ex){/* Empty catch <3 */}

        // Nothing from data types above -> default string
        return DataType.STRING;
    }
}
