package cz.filek.dbm2;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Properties {
    public Map<Property, Resource> dataTypeProperties;
    public Map<Property, Resource> objectProperties;

    public Properties(){
        dataTypeProperties = new HashMap<>();
        objectProperties = new HashMap<>();
    }

    public ArrayList<Property> getObjectProperties(){
        ArrayList<Property> props = new ArrayList<>();
        for(Property prop : objectProperties.keySet()){
            props.add(prop);
        }
        return props;
    }
}
