package cz.filek.dbm2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static Logger logger = LoggerFactory.getLogger("Main");

    public static void main(String[] args) {
        if (args.length >= 3) {
            long start = System.currentTimeMillis();
            OntologyCreator ontologyCreator;
            if (args[2] != null)
                try {
                    int numIndividuals = Integer.parseInt(args[2]);
                    ontologyCreator = new OntologyCreator(args[0], args[1], numIndividuals);
                } catch (Exception ex) {
                    logger.warn("Zadán špatný parametr pro počet instancí individuálů. Používám výchozí hodnotu 4.");
                    ontologyCreator = new OntologyCreator(args[0], args[1], 4);
                }

            else
                ontologyCreator = new OntologyCreator(args[0], args[1], 4);
            ontologyCreator.createOntology();
            long end = System.currentTimeMillis();
            logger.info("Doba vytváření ontologie " + (end - start) + "ms.");
        } else {
            logger.error("Zadán špatný počet parametrů!");
            logger.info("java -jar ontcreator [vstupní soubor] [výstupní soubor] [počet individuálů]");
        }
    }
}
