package cz.filek.dbm2;

import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.soap.Node;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OntologyCreator {
    private Logger logger = LoggerFactory.getLogger("OntologyCreator");
    private String dataFilePath;
    private String outputFilePath;
    private int individualsMin;

    private Model rdfModel;
    private OntModel ontModel;

    ////////////////////////////
    private int numOfClass = 0;
    private int numOfDatatypes = 0;
    private int numOfObjects = 0;
    ////////////////////////////

    public OntologyCreator(String dataFilePath, String outputFilePath, int individualsMin) {
        this.dataFilePath = dataFilePath;
        this.outputFilePath = outputFilePath;
        this.individualsMin = individualsMin;
    }

    /**
     * Creates ontology model from RDF model
     */
    public void createOntology() {
        try {
            rdfModel = RDFDataMgr.loadModel(dataFilePath);
        } catch (Exception ex) {
            logger.error("Nastala chyba při načítání RDF modelu: " + ex.getMessage());
        }

        if (rdfModel == null) {
            return;
        }

        ontModel = ModelFactory.createOntologyModel();

        // Set NameSpace map from RDF file
        ontModel.setNsPrefixes(rdfModel.getNsPrefixMap());


        // Information about created ontology
        String ontName = new File(dataFilePath).getName();
        Ontology ontology = ontModel.createOntology("http://fifal.cz/ontology-" + ontName + ".owl");
        ontology.addComment("Ontologie vygenerovaná ze souboru " + dataFilePath, "cs");

        // Class creation
        logger.info("Načítám třídy rdf modelu.");
        ArrayList<RDFNode> classes = getClasses();
        logger.info("Vytvářím třídy ontologie.");
        for (RDFNode node : classes) {
            OntClass ontClass = ontModel.createClass(node.toString());
            try {
                ontClass.addProperty(RDFS.label, node.asResource().getLocalName());
            } catch (Exception ex) {
            }
            numOfClass++;
        }


        logger.info("Načítám vlastnosti rdf modelu.");
        Properties properties = getProperties();
        logger.info("Vytvářím vlastnosti ontologie.");


        ontModel.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");

        for (Property prop : properties.dataTypeProperties.keySet()) {
            ArrayList<Resource> domainsList = getPropertyDomains(prop);

            DatatypeProperty datatypeProperty = ontModel.createDatatypeProperty(prop.toString());
            for (Resource domain : domainsList) {
                try {
                    datatypeProperty.addDomain(domain.getProperty(RDF.type).getObject().asResource());
                } catch (Exception ex) {
                    // Sometimes getProp.getObject is null...
                }
            }
            if (isFunctionalProperty(prop)) {
                datatypeProperty.addRDFType(OWL.FunctionalProperty);
            }
            datatypeProperty.addRange(properties.dataTypeProperties.get(prop));
            datatypeProperty.addProperty(RDFS.label, prop.getLocalName());
            numOfDatatypes++;
        }

        for (Property prop : properties.objectProperties.keySet()) {
            ArrayList<Resource> domainsList = getPropertyDomains(prop);

            ObjectProperty objectProperty = ontModel.createObjectProperty(prop.toString());
            for (Resource domain : domainsList) {
                try {
                    objectProperty.addDomain(domain.getProperty(RDF.type).getObject().asResource());
                } catch (Exception ex) {
                    // Sometimes getProp.getObject is null...
                }
            }
            if (isFunctionalProperty(prop)) {
                objectProperty.addRDFType(OWL.FunctionalProperty);
            }
            objectProperty.addRange(properties.objectProperties.get(prop));
            objectProperty.addProperty(RDFS.label, prop.getLocalName());
            numOfObjects++;
        }

        findIndividuals(classes);

        logger.info("--------------------------------");
        logger.info("Informace o vytvořené ontologii:");
        logger.info("\t Počet tříd: " + numOfClass);
        logger.info("\t Počet datových vlastností: " + numOfDatatypes);
        logger.info("\t Počet objektových vlastností: " + numOfObjects);
        logger.info("\t Počet individuálů: " + getIndividualsCount());
        logger.info("--------------------------------");

        saveOntologyModel();

        logger.info("Ontologie byla vytvořena.");

    }

    /**
     * Gets array list of all classes in RDF model
     *
     * @return ArrayList<RDFNode>
     */
    private ArrayList<RDFNode> getClasses() {
        ResIterator stmtIterator = rdfModel.listResourcesWithProperty(RDF.type);
        ArrayList<RDFNode> classes = new ArrayList<>();

        while (stmtIterator.hasNext()) {
            Resource statement = stmtIterator.nextResource();
            if (statement != null) {
                if (!classes.contains(statement.getProperty(RDF.type).getObject())) {
                    classes.add(statement.getProperty(RDF.type).getObject());
                }
            }
        }

        return classes;
    }

    /**
     * @return
     */
    private Properties getProperties() {
        Map<Property, Resource> dataTypeProperties = new HashMap<>();
        Map<Property, Resource> objectProperties = new HashMap<>();

        StmtIterator stmtIterator = rdfModel.listStatements();
        while (stmtIterator.hasNext()) {
            Statement statement = stmtIterator.nextStatement();
            if (isObjectProperty(statement)) {
                if (!statement.getPredicate().toString().equals(RDF.type.toString())) {
                    objectProperties.put(statement.getPredicate(), getObjectPropertyRange(statement));
                }
            } else {
                if (!statement.getPredicate().toString().equals(RDF.type.toString())) {
                    dataTypeProperties.put(statement.getPredicate(), getDatatypePropertyRange(statement));
                }
            }
        }

        Properties properties = new Properties();
        properties.objectProperties = objectProperties;
        properties.dataTypeProperties = dataTypeProperties;
        return properties;
    }

    /**
     * Checks if statement object is object property or not
     *
     * @param statement
     * @return true if is object property, false otherwise
     */
    private boolean isObjectProperty(Statement statement) {
        return statement != null && statement.getObject() != null && statement.getObject().isResource();
    }

    /**
     * Gets domains of given property
     *
     * @param property
     * @return array list of all domains for given property
     */
    private ArrayList<Resource> getPropertyDomains(Property property) {
        ArrayList<Resource> domains = new ArrayList<>();

        // Všechny třídy subjektů používající property
        ResIterator resIterator = rdfModel.listSubjectsWithProperty(property);
        while (resIterator.hasNext()) {
            Resource domain = resIterator.next();
            if (!domains.contains(domain)) {
                domains.add(domain);
            }
        }

        return domains;
    }

    /**
     * Returns range of given property
     *
     * @param statement
     * @return
     */
    private Resource getDatatypePropertyRange(Statement statement) {
        Utils.DataType dataType = Utils.predictDataType(statement.getObject().toString());

        switch (dataType) {
            case INTEGER:
                return XSD.integer;
            case DOUBLE:
                return XSD.xdouble;
            case STRING:
                return XSD.xstring;
            case LONG:
                return XSD.xlong;
            case URI:
                return XSD.anyURI;
            default:
                return XSD.xstring;

        }
    }

    /**
     * @param statement
     * @return
     */
    private Resource getObjectPropertyRange(Statement statement) {
        ExtendedIterator<OntClass> it = ontModel.listClasses();
        while (it.hasNext()) {
            OntClass oc = it.next();
            if (oc.getLocalName().toLowerCase().equals(statement.getPredicate().getLocalName().toLowerCase())) {
                return oc;
            }
        }

        return XSD.anyURI;
    }

    private Resource getObjectPropertyRange(Property property) {
        ExtendedIterator<OntClass> it = ontModel.listClasses();
        while (it.hasNext()) {
            OntClass oc = it.next();
            if (oc.getLocalName().toLowerCase().equals(property.getLocalName().toLowerCase())) {
                return oc;
            }
        }

        return XSD.anyURI;
    }

    private boolean isFunctionalProperty(Property prop) {
        StmtIterator stmtIterator = rdfModel.listStatements();
        Map<Resource, Integer> map = new HashMap<>();

        while (stmtIterator.hasNext()) {
            Statement statement = stmtIterator.nextStatement();

            if (statement.getPredicate().getLocalName().equals(prop.getLocalName())) {
                if (map.get(statement.getSubject()) == null) {
                    map.put(statement.getSubject(), 1);
                } else {
                    map.replace(statement.getSubject(), map.get(statement.getSubject()) + 1);
                }
            }
        }

        for (Resource r : map.keySet()) {
            if (map.get(r) != 1) {
                return false;
            }
        }

        return true;
    }

    private void findIndividuals(ArrayList<RDFNode> classes) {
        Properties props = getProperties();
        ArrayList<Property> objects = props.getObjectProperties();
        Map<Property, ArrayList<Map<Property, String>>> propMaps = new HashMap<>();

        for (Property prop : objects) {
            int numInstances = 0;
            ResIterator resIterator = rdfModel.listResourcesWithProperty(prop);
            ArrayList<Map<Property, String>> maps = new ArrayList<>();
            while (resIterator.hasNext()) {
                numInstances++;
                Resource res = resIterator.nextResource();
                Map<Property, String> propMap = new HashMap<>();

                StmtIterator stmtIterator = res.getProperty(prop).getObject().asResource().listProperties();

                if (!stmtIterator.hasNext()) {
                    propMap.put(RDF.type, res.getProperty(prop).getObject().toString());
                } else {
                    while (stmtIterator.hasNext()) {
                        Statement stmt = stmtIterator.nextStatement();
                        if (!stmt.getPredicate().equals(RDF.type)) {
                            propMap.put(stmt.getPredicate(), stmt.getObject().toString());
                        }
                    }
                }
                if (!maps.contains(propMap)) {
                    maps.add(propMap);
                }
            }
            if (numInstances / maps.size() >= individualsMin) {
                propMaps.put(prop, maps);
            }
        }
        createIndividuals(propMaps);
    }

    private void createIndividuals(Map<Property, ArrayList<Map<Property, String>>> propMaps) {
        for (Property prop : propMaps.keySet()) {
            for (Map<Property, String> map : propMaps.get(prop)) {
                // Named Individuals
                if (map.get(RDF.type) != null) {
                    Resource res = ontModel.createResource(map.get(RDF.type));
                    res.addProperty(RDFS.label, res.getLocalName());
                    ontModel.createIndividual(map.get(RDF.type), OWL2.NamedIndividual);
                }
                // Class individuals
                else {
                    if (!getObjectPropertyRange(prop).equals(XSD.anyURI)) {
                        OntClass ontClass = ontModel.getOntClass(getObjectPropertyRange(prop).toString());
                        Property firstProp = map.keySet().iterator().next();
                        String firstPropValue = map.get(firstProp);
                        Individual individual = ontModel.createIndividual(ontClass.toString() + "-" + firstPropValue.toLowerCase(), ontClass);

                        for (Property p : map.keySet()) {
                            individual.addProperty(p, map.get(p));
                        }
                    }
                }
            }
        }
    }

    private int getIndividualsCount(){
        int count = 0;

        ExtendedIterator<Individual> it = ontModel.listIndividuals();
        while(it.hasNext()){
            count++;
            it.next();
        }

        StmtIterator stmtIterator = ontModel.listStatements();
        while (stmtIterator.hasNext()){
            Statement stmt = stmtIterator.nextStatement();
            if(stmt.getObject().equals(OWL2.NamedIndividual)){
                count++;
            }
        }

        return count;
    }

    /**
     * Saves created model into file
     */
    private void saveOntologyModel() {
        File output = new File(outputFilePath);
        if (output != null) {
            try {
                FileOutputStream fos = new FileOutputStream(output);
                ontModel.write(fos);
            } catch (FileNotFoundException e) {
                logger.error("Chyba při zapisování ontologického modelu " + e.getMessage());
            }
        }
    }
}
